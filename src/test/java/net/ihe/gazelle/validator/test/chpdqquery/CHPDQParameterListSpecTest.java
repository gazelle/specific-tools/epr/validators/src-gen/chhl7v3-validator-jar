package net.ihe.gazelle.validator.test.chpdqquery;

import org.junit.Assert;
import org.junit.Test;
public class CHPDQParameterListSpecTest {

	PDQQueryTestUtil testExecutor = new PDQQueryTestUtil();

	@Test
	// PatientTelecom is not an allowed parameter in PQD Query. KO.
	public void test_ko_constraint_pdqquery_PatientTelecom() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpdq/query/PDQQuery.CH-PDQ-001.PatientTelecom.xml",
						"chpdqquery-CHPDQParameterListSpec-ch_pdq_001_PatientTelecom"));
	}

	@Test
	// PatientTelecom is not an allowed parameter in PQD Query. OK.
	public void test_ok_constraint_pdqquery_PatientTelecom() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/query/PDQQuery.xml",
						"chpdqquery-CHPDQParameterListSpec-ch_pdq_001_PatientTelecom"));
	}
}