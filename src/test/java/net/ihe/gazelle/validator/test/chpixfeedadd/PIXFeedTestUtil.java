package net.ihe.gazelle.validator.test.chpixfeedadd;

import net.ihe.gazelle.hl7v3.prpain201301UV02.PRPAIN201301UV02Type;
import net.ihe.gazelle.hl7v3.validator.chpixfeedadd.CHPIXFEEDADDPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class PIXFeedTestUtil extends AbstractValidator<PRPAIN201301UV02Type>{

    @Override
    protected void validate(PRPAIN201301UV02Type message,
                            List<Notification> notifications) {
        PRPAIN201301UV02Type.validateByModule(message, "/PRPA_IN201301UV02", new CHPIXFEEDADDPackValidator(), notifications);
    }

    @Override
    protected Class<PRPAIN201301UV02Type> getMessageClass() {
        return PRPAIN201301UV02Type.class;
    }
}
