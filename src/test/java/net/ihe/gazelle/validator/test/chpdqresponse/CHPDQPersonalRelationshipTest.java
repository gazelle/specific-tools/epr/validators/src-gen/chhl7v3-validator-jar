package net.ihe.gazelle.validator.test.chpdqresponse;

import org.junit.Assert;
import org.junit.Test;
public class CHPDQPersonalRelationshipTest {

	PDQResponseTestUtil testExecutor = new PDQResponseTestUtil();

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. KO.
	public void test_ko_constraint_pdqresponse_PersonalRelationship_displayName() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFileWarning(
						"src/test/resources/chpdq/response/PDQResponse.CH-PDQ-013.personalRelationship.xml",
						"chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_displayName"));
	}

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. OK.
	public void test_ok_constraint_pdqresponse_PersonalRelationship_displayName_noDisplayName() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.CH-PDQ-013.personalRelationshipWOdisplayName.xml",
						"chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_displayName"));
	}

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. OK.
	public void test_ok_constraint_pdqresponse_PersonalRelationship_displayName_goodDisplayName() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.xml",
						"chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_displayName"));
	}

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. OK.
	public void test_ok_constraint_pdqresponse_PersonalRelationship() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.xml",
						"chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_Code"));
	}

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. OK.
	public void test_ko_constraint_pdqresponse_PersonalRelationship_badCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpdq/response/PDQResponse.CH-PDQ-013.personalRelationshipWrongCode.xml",
						"chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_Code"));
	}
}