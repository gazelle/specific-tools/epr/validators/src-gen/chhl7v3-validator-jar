package net.ihe.gazelle.validator.test.chxcpdresponse;

import org.junit.Assert;
import org.junit.Test;

public class CHXCPDPersonSpecTest {

    XCPDResponseTestUtil testExecutor = new XCPDResponseTestUtil();

    @Test
    // Telecom is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_Telecom() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-011.telecom.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_011_Telecom"));
    }

    @Test
    // Telecom is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_Telecom() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_011_Telecom"));
    }

    @Test
    // DeceasedInd is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_DeceasedInd() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-014.deceasedInd.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_014_DeceasedInd"));
    }

    @Test
    // DeceasedInd is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_DeceasedInd() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_014_DeceasedInd"));
    }

    @Test
    // DeceasedTime is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_DeceasedTime() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-015.deceasedTime.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_015_DeceasedTime"));
    }

    @Test
    // DeceasedTime is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_DeceasedTime() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_015_DeceasedTime"));
    }

    @Test
    // MultipleBirthInd is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_MultipleBirthInd() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-016.multipleBirthInd.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_016_MultipleBirthInd"));
    }

    @Test
    // MultipleBirthInd is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_MultipleBirthInd() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_016_MultipleBirthInd"));
    }

    @Test
    // MultipleBirthOrderNumber is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_MultipleBirthOrderNumber() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-017.multipleBirthOrderNumber.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_017_MultipleBirthOrderNumber"));
    }

    @Test
    // MultipleBirthOrderNumber is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_MultipleBirthOrderNumber() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_017_MultipleBirthOrderNumber"));
    }

    @Test
    // Addr is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_Addr() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-018.addr.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_018_Addr"));
    }

    @Test
    // Addr is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_Addr() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_018_Addr"));
    }

    @Test
    // MaritalStatusCode is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_MaritalStatusCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-019.maritalStatusCode.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_019_MaritalStatusCode"));
    }

    @Test
    // MaritalStatusCode is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_MaritalStatusCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_019_MaritalStatusCode"));
    }

    @Test
    // ReligiousAffiliationCode is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_ReligiousAffiliationCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-020.religiousAffiliationCode.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_020_ReligiousAffiliationCode"));
    }

    @Test
    // ReligiousAffiliationCode is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_ReligiousAffiliationCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_020_ReligiousAffiliationCode"));
    }

    @Test
    // RaceCode is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_RaceCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-021.raceCode.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_021_RaceCode"));
    }

    @Test
    // RaceCode is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_RaceCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_021_RaceCode"));
    }

    @Test
    // EthnicGroupCode is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_EthnicGroupCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-022.ethnicGroupCode.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_022_EthnicGroupCode"));
    }

    @Test
    // EthnicGroupCode is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_EthnicGroupCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_022_EthnicGroupCode"));
    }

    @Test
    // AsOtherIDs is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_AsOtherIDs() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-023.asOtherIDs.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_023_AsOtherIDs"));
    }

    @Test
    // AsOtherIDs is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_AsOtherIDs() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_023_AsOtherIDs"));
    }

    @Test
    // PersonalRelationship is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_PersonalRelationship() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-025.personalRelationship.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_025_PersonalRelationship"));
    }

    @Test
    // PersonalRelationship is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_PersonalRelationship() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_025_PersonalRelationship"));
    }

    @Test
    // AsCitizen is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_AsCitizen() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-028.asCitizenAndNation.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_028_AsCitizen"));
    }

    @Test
    // AsCitizen is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_AsCitizen() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_028_AsCitizen"));
    }

    @Test
    // AsEmployee is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_AsEmployee() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-034.asEmployee.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_034_AsEmployee"));
    }

    @Test
    // AsEmployee is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_AsEmployee() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_034_AsEmployee"));
    }

    @Test
    // LanguageCommunication is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_LanguageCommunication() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-037.languageCommunication.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_037_LanguageCommunication"));
    }

    @Test
    // LanguageCommunication is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_LanguageCommunication() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_037_LanguageCommunication"));
    }

    @Test
    // The Name(s) fields MAY be null. KO.
    public void test_ko_constraint_xcpdresponse_Name() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-040.name.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_040_Name"));
    }

    @Test
    // The Name(s) fields MAY be null. OK.
    public void test_ok_constraint_xcpdresponse_Name() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_040_Name"));
    }
}