package net.ihe.gazelle.validator.test.chxcpdresponse;

import org.junit.Assert;
import org.junit.Test;

public class CHXCPDQueryMatchObservationSpecTest {

    XCPDResponseTestUtil testExecutor = new XCPDResponseTestUtil();

    @Test
    // The  value  field must be a numeric value between 0 (excluded) and 100 (0 < percentage value <= 100) MUST be used (100 for an exact match). KO.
    public void test_ko_constraint_xcpdresponse_QueryMatchObservation() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-039.queryMatchObservation.xml",
                        "chxcpdresponse-CHXCPDQueryMatchObservationSpec-ch_xcpd_039_QueryMatchObservation"));
    }

    @Test
    // The  value  field must be a numeric value between 0 (excluded) and 100 (0 < percentage value <= 100) MUST be used (100 for an exact match).. OK.
    public void test_ok_constraint_xcpdresponse_QueryMatchObservation() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDQueryMatchObservationSpec-ch_xcpd_039_QueryMatchObservation"));
    }
}