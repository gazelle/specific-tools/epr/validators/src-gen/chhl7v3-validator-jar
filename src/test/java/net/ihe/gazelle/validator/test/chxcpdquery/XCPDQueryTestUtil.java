package net.ihe.gazelle.validator.test.chxcpdquery;

import net.ihe.gazelle.hl7v3.prpain201305UV02.PRPAIN201305UV02Type;
import net.ihe.gazelle.hl7v3.validator.chxcpdquery.CHXCPDQUERYPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class XCPDQueryTestUtil extends AbstractValidator<PRPAIN201305UV02Type> {

	@Override
	protected void validate(PRPAIN201305UV02Type message,
			List<Notification> notifications) {
		PRPAIN201305UV02Type.validateByModule(message, "/PRPA_IN201305UV02", new CHXCPDQUERYPackValidator(), notifications);
	}

	@Override
	protected Class<PRPAIN201305UV02Type> getMessageClass() {
		return PRPAIN201305UV02Type.class;
	}
}
