package net.ihe.gazelle.validator.test.chpixfeedrevise;

import org.junit.Assert;
import org.junit.Test;

public class CHPIXPatientSpecTest {

    PIXFeedTestUtil testExecutor = new PIXFeedTestUtil();

    @Test
    // At least "Person.name" or "Patient.id" must be non-null. KO.
    public void test_ko_constraint_pixfeed_NameOrID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-001.nameOrID.xml",
                        "chpixfeedrevise-CHPIXRevisePatientSpec-ch_pix_001_revise_NameOrId"));
    }

    @Test
    // At least "Person.name" or "Patient.id" must be non-null. OK.
    public void test_ok_constraint_pixfeed_NameOrID_both() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.xml",
                        "chpixfeedrevise-CHPIXRevisePatientSpec-ch_pix_001_revise_NameOrId"));
    }

    @Test
    // At least "Person.name" or "Patient.id" must be non-null. OK.
    public void test_ok_constraint_pixfeed_NameOrID_name() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.name.xml",
                        "chpixfeedrevise-CHPIXRevisePatientSpec-ch_pix_001_revise_NameOrId"));
    }

    @Test
    // At least "Person.name" or "Patient.id" must be non-null. OK.
    public void test_ok_constraint_pixfeed_NameOrID_id() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.id.xml",
                        "chpixfeedrevise-CHPIXRevisePatientSpec-ch_pix_001_revise_NameOrId"));
    }

}		
