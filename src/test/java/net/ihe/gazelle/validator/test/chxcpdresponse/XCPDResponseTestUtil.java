package net.ihe.gazelle.validator.test.chxcpdresponse;

import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.validator.chxcpdresponse.CHXCPDRESPONSEPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class XCPDResponseTestUtil extends AbstractValidator<PRPAIN201306UV02Type> {

    @Override
    protected void validate(PRPAIN201306UV02Type message,
                            List<Notification> notifications) {
        PRPAIN201306UV02Type.validateByModule(message, "/PRPA_IN201305UV02", new CHXCPDRESPONSEPackValidator(), notifications);
    }

    @Override
    protected Class<PRPAIN201306UV02Type> getMessageClass() {
        return PRPAIN201306UV02Type.class;
    }
}
