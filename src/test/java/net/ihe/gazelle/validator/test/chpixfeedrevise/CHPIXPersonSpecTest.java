package net.ihe.gazelle.validator.test.chpixfeedrevise;

import org.junit.Assert;
import org.junit.Test;

public class CHPIXPersonSpecTest {

    PIXFeedTestUtil testExecutor = new PIXFeedTestUtil();

    @Test
    // RaceCode is not an allowed parameter in PDQ Response. KO.
    public void test_ko_constraint_pixfeed_RaceCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-004.raceCode.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_004_revise_RaceCode"));
    }

    @Test
    // RaceCode is not an allowed parameter in PDQ Response. OK.
    public void test_ok_constraint_pixfeed_RaceCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_004_revise_RaceCode"));
    }

    @Test
    // ReligiousAffiliationCode is not an allowed parameter in PDQ Response. KO.
    public void test_ko_constraint_pixfeed_ReligiousAffiliationCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-003.religiousAffiliationCode.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_003_revise_ReligiousAffiliationCode"));
    }

    @Test
    // ReligiousAffiliationCode is not an allowed parameter in PDQ Response. OK.
    public void test_ok_constraint_pixfeed_ReligiousAffiliationCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_003_revise_ReligiousAffiliationCode"));
    }

    @Test
    // EthnicGroupCode is not an allowed parameter in PDQ Response. KO.
    public void test_ko_constraint_pixfeed_EthnicGroupCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-005.ethnicGroupCode.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_005_revise_EthnicGroupCode"));
    }

    @Test
    // EthnicGroupCode is not an allowed parameter in PDQ Response. OK.
    public void test_ok_constraint_pixfeed_EthnicGroupCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_005_revise_EthnicGroupCode"));
    }

    @Test
    // In the "name" field, the birth name is passed with the qualifier BR. KO.
    public void test_ko_constraint_pixfeed_NameWithoutQualifier() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-002.nameWithoutQualifier.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_002_revise_Name"));
    }

    @Test
    // In the "name" field, the birth name is passed with the qualifier BR. KO.
    public void test_ko_constraint_pixfeed_NameWithWrongQualifier() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-002.nameWithWrongQualifier.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_002_revise_Name"));
    }

    @Test
    // In the "name" field, the birth name is passed with the qualifier BR. OK.
    public void test_ok_constraint_pixfeed_Name() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.xml",
                        "chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_002_revise_Name"));
    }
}