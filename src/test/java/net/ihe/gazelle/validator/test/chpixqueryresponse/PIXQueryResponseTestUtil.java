package net.ihe.gazelle.validator.test.chpixqueryresponse;

import net.ihe.gazelle.hl7v3.prpain201310UV02.PRPAIN201310UV02Type;
import net.ihe.gazelle.hl7v3.validator.chpixqueryresponse.CHPIXQUERYRESPONSEPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class PIXQueryResponseTestUtil extends AbstractValidator<PRPAIN201310UV02Type> {

    @Override
    protected void validate(PRPAIN201310UV02Type message,
                            List<Notification> notifications) {
        PRPAIN201310UV02Type.validateByModule(message, "/PRPA_IN201310UV02", new CHPIXQUERYRESPONSEPackValidator(), notifications);
    }

    @Override
    protected Class<PRPAIN201310UV02Type> getMessageClass() {
        return PRPAIN201310UV02Type.class;
    }
}


