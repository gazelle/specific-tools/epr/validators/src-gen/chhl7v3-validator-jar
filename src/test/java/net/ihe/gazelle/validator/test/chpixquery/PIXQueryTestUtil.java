package net.ihe.gazelle.validator.test.chpixquery;

import java.util.List;

import net.ihe.gazelle.hl7v3.prpain201309UV02.PRPAIN201309UV02Type;
import net.ihe.gazelle.hl7v3.validator.chpixquery.CHPIXQUERYPackValidator;
import net.ihe.gazelle.validator.test.common.AbstractValidator;
import net.ihe.gazelle.validation.Notification;

public class PIXQueryTestUtil extends AbstractValidator<PRPAIN201309UV02Type> {

    @Override
    protected void validate(PRPAIN201309UV02Type controlActProcess,
                            List<Notification> notifications) {
        PRPAIN201309UV02Type.validateByModule(controlActProcess,
                "/PRPA_IN201309UV02", new CHPIXQUERYPackValidator(),
                notifications);
    }

    @Override
    protected Class<PRPAIN201309UV02Type> getMessageClass() {
        return PRPAIN201309UV02Type.class;
    }
}