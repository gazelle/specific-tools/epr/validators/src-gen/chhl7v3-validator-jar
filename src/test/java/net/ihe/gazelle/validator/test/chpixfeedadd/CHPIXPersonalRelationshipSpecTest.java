package net.ihe.gazelle.validator.test.chpixfeedadd;

import org.junit.Assert;
import org.junit.Test;

public class CHPIXPersonalRelationshipSpecTest {

	PIXFeedTestUtil testExecutor = new PIXFeedTestUtil();

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. KO.
	public void test_ko_constraint_pixfeed_PersonalRelationshipFather() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-007.personalRelationshipFather.xml",
                        "chpixfeedadd-CHPIXAddPersonalRelationshipSpec-ch_pix_007_add_Code"));
    }

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. KO.
	public void test_ko_constraint_pixfeed_PersonalRelationshipMother() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-007.personalRelationshipMother.xml",
                        "chpixfeedadd-CHPIXAddPersonalRelationshipSpec-ch_pix_007_add_Code"));
    }

	@Test
	// The code MUST be "FTH" for the Father and "MTH" for the Mother. OK.
	public void test_ok_constraint_pixfeed_PersonalRelationship() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.xml",
                        "chpixfeedadd-CHPIXAddPersonalRelationshipSpec-ch_pix_007_add_Code"));
    }
}