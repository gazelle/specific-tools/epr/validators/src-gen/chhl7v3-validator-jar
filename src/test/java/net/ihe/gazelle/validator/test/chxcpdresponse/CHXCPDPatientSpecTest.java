package net.ihe.gazelle.validator.test.chxcpdresponse;

import org.junit.Assert;
import org.junit.Test;

public class CHXCPDPatientSpecTest {

    XCPDResponseTestUtil testExecutor = new XCPDResponseTestUtil();

    @Test
    // "Patient.id" is mandatory. KO.
    public void test_ko_constraint_xcpdresponse_ID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-010.idnull.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_010_Id"));
    }

    @Test
    // "Patient.id" is mandatory. OK.
    public void test_ok_constraint_xcpdresponse_ID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_010_Id"));
    }

    @Test
    // ConfidentialityCode is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_ConfidentialityCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-008.confidentialityCode.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_008_ConfidentialityCode"));
    }

    @Test
    // ConfidentialityCode is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_ConfidentialityCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_008_ConfidentialityCode"));
    }

    @Test
    // VeryImportantPersonCode is not an allowed parameter in  XCPD Response. KO.
    public void test_ko_constraint_xcpdresponse_VeryImportantPersonCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-009.veryImportantPersonCode.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_009_VeryImportantPersonCode"));
    }

    @Test
    // VeryImportantPersonCode is not an allowed parameter in  XCPD Response. OK.
    public void test_ok_constraint_xcpdresponse_VeryImportantPersonCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_009_VeryImportantPersonCode"));
    }

    @Test
    // The MPI-PID MUST be returned if there is a match from the EPR-PID. KO.
    public void test_ko_constraint_xcpdresponse_Mpipid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.CH-XCPD-007.mpipid.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_007_Mpipid"));
    }

    @Test
    // The MPI-PID MUST be returned if there is a match from the EPR-PID. OK.
    public void test_ok_constraint_xcpdresponse_Mpipid() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/response/XcpdResponse.xml",
                        "chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_007_Mpipid"));
    }
}
