package net.ihe.gazelle.validator.test.chpdqresponse;

import org.junit.Assert;
import org.junit.Test;

public class CHPDQPersonSpecTest {

    PDQResponseTestUtil testExecutor = new PDQResponseTestUtil();

    @Test
    // RaceCode is not an allowed parameter in PDQ Response. KO.
    public void test_ko_constraint_pdqresponse_RaceCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-010.raceCode.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_010_RaceCode"));
    }

    @Test
    // RaceCode is not an allowed parameter in PDQ Response. OK.
    public void test_ok_constraint_pdqresponse_RaceCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_010_RaceCode"));
    }

    @Test
    // EthnicGroupCode is not an allowed parameter in PDQ Response. KO.
    public void test_ko_constraint_pdqresponse_EthnicGroupCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-011.ethnicGroupCode.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_011_EthnicGroupCode"));
    }

    @Test
    // EthnicGroupCode is not an allowed parameter in PDQ Response. OK.
    public void test_ok_constraint_pdqresponse_EthnicGroupCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_011_EthnicGroupCode"));
    }

    @Test
    // ReligiousAffiliationCode is not an allowed parameter in PDQ Response. KO.
    public void test_ko_constraint_pdqresponse_ReligiousAffiliationCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-009.religiousAffiliationCode.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_009_ReligiousAffiliationCode"));
    }

    @Test
    // ReligiousAffiliationCode is not an allowed parameter in PDQ Response. OK.
    public void test_ok_constraint_pdqresponse_ReligiousAffiliationCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_009_ReligiousAffiliationCode"));
    }

    @Test
    //  In the "name" field, the birth name is passed with the qualifier BR. KO.
    public void test_ko_constraint_pdqresponse_NameWithoutQualifier() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-008.nameWithoutQualifier.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_008_Name"));
    }

    @Test
    //  In the "name" field, the birth name is passed with the qualifier BR. KO.
    public void test_ko_constraint_pdqresponse_NameWithWrongQualifier() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-008.nameWithWrongQualifier.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_008_Name"));
    }

    @Test
    //  In the "name" field, the birth name is passed with the qualifier BR. OK.
    public void test_ok_constraint_pdqresponse_Name() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.xml",
                        "chpdqresponse-CHPDQPersonSpec-ch_pdq_008_Name"));
    }
}