package net.ihe.gazelle.validator.test.chpdqresponse;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02Type;
import net.ihe.gazelle.hl7v3.validator.chpdqresponse.CHPDQRESPONSEPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class PDQResponseTestUtil extends AbstractValidator<PRPAIN201306UV02Type> {

    static {
        CommonOperations.setValueSetProvider(new SVSConsumer() {
            @Override
            protected String getSVSRepositoryUrl() {
                return "https://bcu-indus.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
            }
        });
    }

    @Override
    protected void validate(PRPAIN201306UV02Type message,
                            List<Notification> notifications) {
        PRPAIN201306UV02Type.validateByModule(message, "/PRPA_IN201306UV02", new CHPDQRESPONSEPackValidator(), notifications);
    }

    @Override
    protected Class<PRPAIN201306UV02Type> getMessageClass() {
        return PRPAIN201306UV02Type.class;
    }
}
