package net.ihe.gazelle.validator.test.chpixfeedadd;

import org.junit.Assert;
import org.junit.Test;

public class CHPIXPatientSpecTest {

	PIXFeedTestUtil testExecutor = new PIXFeedTestUtil();

	@Test
	// At least "Person.name" or "Patient.id" must be non-null. KO.
	public void test_ko_constraint_pixfeed_NameOrID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-001.nameOrID.xml",
                        "chpixfeedadd-CHPIXAddPatientSpec-ch_pix_001_add_NameOrId"));
    }

	@Test
	// At least "Person.name" or "Patient.id" must be non-null. OK.
	public void test_ok_constraint_pixfeed_NameOrID_both() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.xml",
                        "chpixfeedadd-CHPIXAddPatientSpec-ch_pix_001_add_NameOrId"));
    }

	@Test
	// At least "Person.name" or "Patient.id" must be non-null. OK.
	public void test_ok_constraint_pixfeed_NameOrID_name() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.name.xml",
                        "chpixfeedadd-CHPIXAddPatientSpec-ch_pix_001_add_NameOrId"));
    }

	@Test
	// At least "Person.name" or "Patient.id" must be non-null. OK.
	public void test_ok_constraint_pixfeed_NameOrID_id() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.id.xml",
                        "chpixfeedadd-CHPIXAddPatientSpec-ch_pix_001_add_NameOrId"));
    }
}		
