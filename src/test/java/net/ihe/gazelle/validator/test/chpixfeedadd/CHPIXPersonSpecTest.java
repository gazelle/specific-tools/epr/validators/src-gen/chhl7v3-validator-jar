package net.ihe.gazelle.validator.test.chpixfeedadd;

import org.junit.Assert;
import org.junit.Test;

public class CHPIXPersonSpecTest {

	PIXFeedTestUtil testExecutor = new PIXFeedTestUtil();

	@Test
	// RaceCode is not an allowed parameter in PDQ Response. KO.
	public void test_ko_constraint_pixfeed_RaceCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-004.raceCode.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_004_add_RaceCode"));
    }

	@Test
	// RaceCode is not an allowed parameter in PDQ Response. OK.
	public void test_ok_constraint_pixfeed_RaceCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_004_add_RaceCode"));
    }

	@Test
	// ReligiousAffiliationCode is not an allowed parameter in PDQ Response. KO.
	public void test_ko_constraint_pixfeed_ReligiousAffiliationCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-003.religiousAffiliationCode.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_003_add_ReligiousAffiliationCode"));
    }

	@Test
	// ReligiousAffiliationCode is not an allowed parameter in PDQ Response. OK.
	public void test_ok_constraint_pixfeed_ReligiousAffiliationCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_003_add_ReligiousAffiliationCode"));
    }

	@Test
	// EthnicGroupCode is not an allowed parameter in PDQ Response. KO.
	public void test_ko_constraint_pixfeed_EthnicGroupCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-005.ethnicGroupCode.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_005_add_EthnicGroupCode"));
    }

	@Test
	// EthnicGroupCode is not an allowed parameter in PDQ Response. OK.
	public void test_ok_constraint_pixfeed_EthnicGroupCode() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_005_add_EthnicGroupCode"));
    }

	@Test
	// In the "name" field, the birth name is passed with the qualifier BR. KO.
	public void test_ko_constraint_pixfeed_NameWithoutQualifier() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-002.nameWithoutQualifier.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_002_add_Name"));
    }

	@Test
	// In the "name" field, the birth name is passed with the qualifier BR. KO.
	public void test_ko_constraint_pixfeed_NameWithWrongQualifier() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpix/feedadd/PixFeed.CH-PIX-002.nameWithWrongQualifier.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_002_add_Name"));
    }

	@Test
	// In the "name" field, the birth name is passed with the qualifier BR. OK.
	public void test_ok_constraint_pixfeed_Name() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedadd/PixFeed.xml",
                        "chpixfeedadd-CHPIXAddPersonSpec-ch_pix_002_add_Name"));
    }
}