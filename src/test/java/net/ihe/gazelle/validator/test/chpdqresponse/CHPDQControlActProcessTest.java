package net.ihe.gazelle.validator.test.chpdqresponse;

import org.junit.Assert;
import org.junit.Test;

public class CHPDQControlActProcessTest {

    PDQResponseTestUtil testExecutor = new PDQResponseTestUtil();

    @Test
    // If there are more than 5 matches a special handling like in the XCPD transaction (see IHE ITI TF-2b, chapter 3.55.4.2.2.6) is necessary KO.
    public void test_ko_constraint_pdqresponse_SpecialHandling() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-014.specialHandling.xml",
                        "chpdqresponse-CHPDQControlActProcess-ch_pdq_014_controlActProcess"));
    }

    @Test
    // If there are more than 5 matches a special handling like in the XCPD transaction (see IHE ITI TF-2b, chapter 3.55.4.2.2.6) is necessary. OK.
    public void test_ok_constraint_pdqresponse_SpecialHandling_WithtoutSubject() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.withoutSubjects.xml",
                        "chpdqresponse-CHPDQControlActProcess-ch_pdq_014_controlActProcess"));
    }

    @Test
    // If there are more than 5 matches a special handling like in the XCPD transaction (see IHE ITI TF-2b, chapter 3.55.4.2.2.6) is necessary. OK.
    public void test_ok_constraint_pdqresponse_SpecialHandling_None() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.xml",
                        "chpdqresponse-CHPDQControlActProcess-ch_pdq_014_controlActProcess"));
    }


    // If there are more than 5 matches a special handling like in the XCPD transaction (see IHE ITI TF-2b, chapter 3.55.4.2.2.6) is necessary. OK.
    public void test_ok_constraint_pdqresponse_SpecialHandling() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.SpecialHandling.xml",
                        "chpdqresponse-CHPDQControlActProcess-ch_pdq_014_controlActProcess"));
    }
}
