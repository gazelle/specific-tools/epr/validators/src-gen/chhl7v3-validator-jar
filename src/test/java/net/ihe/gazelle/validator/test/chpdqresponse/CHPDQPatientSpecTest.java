package net.ihe.gazelle.validator.test.chpdqresponse;

import org.junit.Assert;
import org.junit.Test;
public class CHPDQPatientSpecTest {

	PDQResponseTestUtil testExecutor = new PDQResponseTestUtil();

	@Test
	// Either "Person.name" or "Patient.id" must be non-null. KO.
	public void test_ko_constraint_pdqresponse_NameOrID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpdq/response/PDQResponse.CH-PDQ-007.nameOrID.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_007_NameOrId"));
	}

	@Test
	// Either "Person.name" or "Patient.id" must be non-null. OK.
	public void test_ok_constraint_pdqresponse_NameOrID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_007_NameOrId"));
	}

	@Test
	// Either "Person.name" or "Patient.id" must be non-null. OK.
	public void test_ok_constraint_pdqresponse_NameOrID_id() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.id.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_007_NameOrId"));
	}

	@Test
	// Either "Person.name" or "Patient.id" must be non-null. OK.
	public void test_ok_constraint_pdqresponse_NameOrID_name() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.name.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_007_NameOrId"));
	}

	/* Those test have been removed
	@Test
	// The extension attribut of ID should be valued with the patient EPR-PID. KO.
	public void test_ko_constraint_pdqresponse_ID_EPRPID_size() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpdq/response/PDQResponse.CH-PDQ-006.IdEprpidSize.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_006_Id"));
	}

	@Test
	// The extension attribut of ID should be valued with the patient EPR-PID. KO.
	public void test_ko_constraint_pdqresponse_ID_EPRPID_value() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpdq/response/PDQResponse.CH-PDQ-006.IdEprpidValue.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_006_Id"));
	}


	@Test
	// The extension attribut of ID should be valued with the patient EPR-PID. OK.
	public void test_ok_constraint_pdqresponse_ID_EPRPID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpdq/response/PDQResponse.xml",
						"chpdqresponse-CHPDQPatientSpec-ch_pdq_006_Id"));
	}*/
}