package net.ihe.gazelle.validator.test.chpixfeedrevise;

import net.ihe.gazelle.hl7v3.prpain201302UV02.PRPAIN201302UV02Type;
import net.ihe.gazelle.hl7v3.validator.chpixfeedrevise.CHPIXFEEDREVISEPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;

import java.util.List;

public class PIXFeedTestUtil extends AbstractValidator<PRPAIN201302UV02Type> {

    @Override
    protected void validate(PRPAIN201302UV02Type message,
                            List<Notification> notifications) {
        PRPAIN201302UV02Type.validateByModule(message, "/PRPA_IN201302UV02", new CHPIXFEEDREVISEPackValidator(), notifications);
    }

    @Override
    protected Class<PRPAIN201302UV02Type> getMessageClass() {
        return PRPAIN201302UV02Type.class;
    }
}
