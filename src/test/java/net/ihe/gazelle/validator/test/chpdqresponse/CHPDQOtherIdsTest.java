package net.ihe.gazelle.validator.test.chpdqresponse;

import org.junit.Assert;
import org.junit.Test;

public class CHPDQOtherIdsTest {

    PDQResponseTestUtil testExecutor = new PDQResponseTestUtil();

    @Test
    // In the "OtherIDs" parameter, the EPR-PID MAY be added here. KO.
    public void test_ko_constraint_pdqresponse_EPRPID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chpdq/response/PDQResponse.CH-PDQ-012.EPRPID.xml",
                        "chpdqresponse-CHPDQOtherIds-ch_pdq_012_EPRPID"));
    }

    @Test
    // In the "OtherIDs" parameter, the EPR-PID MAY be added here. OK.
    public void test_ok_constraint_pdqresponse_EPRPID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpdq/response/PDQResponse.xml",
                        "chpdqresponse-CHPDQOtherIds-ch_pdq_012_EPRPID"));
    }
}
