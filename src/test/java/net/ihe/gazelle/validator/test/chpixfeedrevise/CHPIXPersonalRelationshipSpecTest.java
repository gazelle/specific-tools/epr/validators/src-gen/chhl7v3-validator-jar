package net.ihe.gazelle.validator.test.chpixfeedrevise;

import org.junit.Assert;
import org.junit.Test;

public class CHPIXPersonalRelationshipSpecTest {

    PIXFeedTestUtil testExecutor = new PIXFeedTestUtil();

    @Test
    // The code MUST be "FTH" for the Father and "MTH" for the Mother. KO.
    public void test_ko_constraint_pixfeed_PersonalRelationshipFather() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-007.personalRelationshipFather.xml",
                        "chpixfeedrevise-CHPIXRevisePersonalRelationshipSpec-ch_pix_007_revise_Code"));
    }

    @Test
    // The code MUST be "FTH" for the Father and "MTH" for the Mother. KO.
    public void test_ko_constraint_pixfeed_PersonalRelationshipMother() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.CH-PIX-007.personalRelationshipMother.xml",
                        "chpixfeedrevise-CHPIXRevisePersonalRelationshipSpec-ch_pix_007_revise_Code"));
    }

    @Test
    // The code MUST be "FTH" for the Father and "MTH" for the Mother. OK.
    public void test_ok_constraint_pixfeed_PersonalRelationship() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chpix/feedrevise/PixFeed.xml",
                        "chpixfeedrevise-CHPIXRevisePersonalRelationshipSpec-ch_pix_007_revise_Code"));
    }
}