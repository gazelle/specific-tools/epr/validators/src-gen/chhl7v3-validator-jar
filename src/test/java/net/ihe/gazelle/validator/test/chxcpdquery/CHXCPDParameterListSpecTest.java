package net.ihe.gazelle.validator.test.chxcpdquery;

import org.junit.Assert;
import org.junit.Test;

public class CHXCPDParameterListSpecTest {

    XCPDQueryTestUtil testExecutor = new XCPDQueryTestUtil();

    @Test
    // LivingSubjectId is mandatory in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectId"));
    }

    @Test
    // LivingSubjectId is mandatory in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectId.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectId"));
    }

    @Test
    // LivingSubjectAdministrativeGender may be in XCPD Query. OK.
    public void test_ko_constraint_xcpdquery_LivingSubjectAdministrativeGender() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectAdministrativeGender.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectAdministrativeGender"));
    }

    @Test
    // LivingSubjectAdministrativeGender may be in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectAdministrativeGender() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectAdministrativeGender"));
    }

    @Test
    // LivingSubjectAdministrativeGender is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectBirthPlaceAddress() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectBirthPlaceAddress.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthPlaceAddress"));
    }

    @Test
    // LivingSubjectAdministrativeGender is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectBirthPlaceAddress() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthPlaceAddress"));
    }

    @Test
    // LivingSubjectBirthPlaceName is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectBirthPlaceName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectBirthPlaceName.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthPlaceName"));
    }

    @Test
    // LivingSubjectBirthPlaceName is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectBirthPlaceName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthPlaceName"));
    }

    @Test
    // LivingSubjectBirthTime may be in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectBirthTime() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectBirthTime.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthTime"));
    }

    @Test
    // LivingSubjectBirthTime may be in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectBirthTime() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthTime"));
    }

    @Test
    // LivingSubjectDeceasedTime is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectDeceasedTime() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectDeceasedTime.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectDeceasedTime"));
    }

    @Test
    // LivingSubjectDeceasedTime is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectDeceasedTime() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectDeceasedTime"));
    }

    @Test
    // LivingSubjectName may be XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.livingSubjectName.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectName"));
    }

    @Test
    // LivingSubjectName may be XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileInfo(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectName"));
    }

    @Test
    // MothersMaidenName is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_MothersMaidenName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.mothersMaidenName.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_MothersMaidenName"));
    }

    @Test
    // MothersMaidenName is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_MothersMaidenName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_MothersMaidenName"));
    }

    @Test
    // OtherIDsScopingOrganization is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_OtherIDsScopingOrganization() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.otherIDsScopingOrganization.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_OtherIDsScopingOrganization"));
    }

    @Test
    // OtherIDsScopingOrganization is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_OtherIDsScopingOrganization() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_OtherIDsScopingOrganization"));
    }

    @Test
    // PatientAddress is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_PatientAddress() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.patientAddress.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientAddress"));
    }

    @Test
    // PatientAddress is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_PatientAddress() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientAddress"));
    }

    @Test
    // PatientStatusCode is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_PatientStatusCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.patientStatusCode.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientStatusCode"));
    }

    @Test
    // PatientStatusCode is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_PatientStatusCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientStatusCode"));
    }

    @Test
    // PatientTelecom is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_PatientTelecom() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.patientTelecom.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientTelecom"));
    }

    @Test
    // PatientTelecom is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_PatientTelecom() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientTelecom"));
    }

    @Test
    // PrincipalCareProviderId is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_PrincipalCareProviderId() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.principalCareProviderId.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PrincipalCareProviderId"));
    }

    @Test
    // PrincipalCareProviderId is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_PrincipalCareProviderId() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PrincipalCareProviderId"));
    }

    @Test
    // PrincipalCareProvisionId is not an allowed parameter in XCPD Query. KO.
    public void test_ko_constraint_xcpdquery_PrincipalCareProvisionId() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-004.principalCareProvisionId.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PrincipalCareProvisionId"));
    }

    @Test
    // PrincipalCareProvisionId is not an allowed parameter in XCPD Query. OK.
    public void test_ok_constraint_xcpdquery_PrincipalCareProvisionId() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PrincipalCareProvisionId"));
    }

    @Test
    //The LivingSubjectId Parameter MUST contain the EPR-PID in XCPQ Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectIdWithWrongExtension() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileWarning(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-005.livingSubjectIdWithWrongExtension.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_005_LivingSubjectId"));
    }

    @Test
    //The LivingSubjectId Parameter MUST contain the EPR-PID in XCPQ Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectIdWithWrongRoot() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileWarning(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-005.livingSubjectIdWithWrongRoot.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_005_LivingSubjectId"));
    }

    @Test
    //The LivingSubjectId Parameter MUST contain the EPR-PID in XCPQ Query. KO.
    public void test_ko_constraint_xcpdquery_LivingSubjectIdNotPresent() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFileWarning(
                        "src/test/resources/chxcpd/query/XcpdQuery.CH-XCPD-005.livingSubjectIdNotPresent.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_005_LivingSubjectId"));
    }

    @Test
    //The LivingSubjectId Parameter MUST contain the EPR-PID in XCPQ Query. OK.
    public void test_ok_constraint_xcpdquery_LivingSubjectId() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/chxcpd/query/XcpdQuery.xml",
                        "chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_005_LivingSubjectId"));
    }
}