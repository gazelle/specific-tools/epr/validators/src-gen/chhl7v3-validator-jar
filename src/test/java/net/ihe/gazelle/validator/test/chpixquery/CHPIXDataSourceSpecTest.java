package net.ihe.gazelle.validator.test.chpixquery;

import org.junit.Assert;
import org.junit.Test;

/**
 * <p>CHPIXParameterListSpecTest class.</p>
 *
 * @author aberge
 * @version 1.0: 10/11/17
 */

public class CHPIXDataSourceSpecTest {
	PIXQueryTestUtil testExecutor = new PIXQueryTestUtil();

	@Test
	// The "DataSource" Parameter MUST be specified to the assigning authority/authorities of the MPI-PID in the affinity domain. See also ITI TF-2b, chapter 3.45.4.1.2.1. KO.
	public void test_ko_constraint_ch_pix_008_Datasource() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpix/query/PixQuery.CH-PIX-008.datasource.xml",
						"chpixquery-CHPIXDataSourceSpec-ch_pix_008_MpipidOrEprSpid"));
	}

	@Test
	// The "DataSource" Parameter MUST be specified to the assigning authority/authorities of the MPI-PID in the affinity domain. See also ITI TF-2b, chapter 3.45.4.1.2.1. KO.
	public void test_ko_constraint_ch_pix_008_DatasourceNotEPRSPID() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpix/query/PixQuery.CH-PIX-008.datasourceNotEPRSPID.xml",
						"chpixquery-CHPIXDataSourceSpec-ch_pix_008_MpipidOrEprSpid"));
	}

	@Test
	// The "DataSource" Parameter MUST be specified to the assigning authority/authorities of the MPI-PID in the affinity domain. See also ITI TF-2b, chapter 3.45.4.1.2.1. KO.
	public void test_ko_constraint_ch_pix_008_DatasourceTwice() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnNonValidFile(
						"src/test/resources/chpix/query/PixQuery.CH-PIX-008.datasourcePresentTwice.xml",
						"chpixquery-CHPIXDataSourceSpec-ch_pix_008_MpipidOrEprSpid"));
	}

	@Test
	// The "DataSource" Parameter MUST be specified to the assigning authority/authorities of the MPI-PID in the affinity domain. See also ITI TF-2b, chapter 3.45.4.1.2.1. OK.
	public void test_ok_constraint_ch_pix_008_2Datasources() {
		Assert.assertTrue(testExecutor
				.checkConstraintOnValidFile(
						"src/test/resources/chpix/query/PixQuery.CH-PIX-008.2datasources.xml",
						"chpixquery-CHPIXDataSourceSpec-ch_pix_008_MpipidOrEprSpid"));
	}

}