package net.ihe.gazelle.hl7v3.validator.chpdqquery;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





 /**
  * class :        CHPDQParameterListSpec
  * package :   chpdqquery
  * Constraint Spec Class
  * class of test : PRPAMT201306UV02ParameterList
  * 
  */
public final class CHPDQParameterListSpecValidator{


    private CHPDQParameterListSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_001_PatientTelecom
	* PatientTelecom is not an allowed parameter in PDQ Query.
	*
	*/
	private static boolean _validateCHPDQParameterListSpec_Ch_pdq_001_PatientTelecom(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientTelecom())).equals(new Integer(0));
		
	}

	/**
	* Validation of class-constraint : CHPDQParameterListSpec
    * Verify if an element of type CHPDQParameterListSpec can be validated by CHPDQParameterListSpec
	*
	*/
	public static boolean _isCHPDQParameterListSpec(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQParameterListSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList
     * 
     */
    public static void _validateCHPDQParameterListSpec(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQParameterListSpec(aClass)){
			executeCons_CHPDQParameterListSpec_Ch_pdq_001_PatientTelecom(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQParameterListSpec_Ch_pdq_001_PatientTelecom(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQParameterListSpec_Ch_pdq_001_PatientTelecom(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_001_PatientTelecom");
		notif.setDescription("PatientTelecom is not an allowed parameter in PDQ Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqquery-CHPDQParameterListSpec-ch_pdq_001_PatientTelecom");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-001"));
		diagnostic.add(notif);
	}

}
