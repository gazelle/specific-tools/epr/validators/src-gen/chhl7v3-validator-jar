package net.ihe.gazelle.hl7v3.validator.chpixfeedrevise;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDsId;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientId;



public class CHPIXFEEDREVISEPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHPIXFEEDREVISEPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient){
			net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient aClass = ( net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient)obj;
			CHPIXRevisePatientSpecValidator._validateCHPIXRevisePatientSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person){
			net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass = ( net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person)obj;
			CHPIXRevisePersonSpecValidator._validateCHPIXRevisePersonSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship){
			net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship aClass = ( net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship)obj;
			CHPIXRevisePersonalRelationshipSpecValidator._validateCHPIXRevisePersonalRelationshipSpec(aClass, location, diagnostic);
		}
	
	}

}

