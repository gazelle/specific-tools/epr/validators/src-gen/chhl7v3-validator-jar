package net.ihe.gazelle.hl7v3.validator.chpixquery;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02DataSource;



public class CHPIXQUERYPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHPIXQUERYPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList){
			net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList aClass = ( net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList)obj;
			CHPIXDataSourceSpecValidator._validateCHPIXDataSourceSpec(aClass, location, diagnostic);
		}
	
	}

}

