package net.ihe.gazelle.hl7v3.validator.chpixfeedadd;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPIXAddPatientSpec
  * package :   chpixfeedadd
  * Constraint Spec Class
  * class of test : PRPAMT201301UV02Patient
  * 
  */
public final class CHPIXAddPatientSpecValidator{


    private CHPIXAddPatientSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_001_add_NameOrId
	* At least  Person.name  or  Patient.id  must be non-null.
	*
	*/
	private static boolean _validateCHPIXAddPatientSpec_Ch_pix_001_add_NameOrId(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!!(anElement1.getNullFlavor() == null)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		Boolean result2;
		result2 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement2 : aClass.getPatientPerson().getName()) {
			    if (!!(anElement2.getNullFlavor() == null)) {
			        result2 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientPerson().getName()) > new Integer(0))) && !(result1 && result2));
		
		
	}

	/**
	* Validation of class-constraint : CHPIXAddPatientSpec
    * Verify if an element of type CHPIXAddPatientSpec can be validated by CHPIXAddPatientSpec
	*
	*/
	public static boolean _isCHPIXAddPatientSpec(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXAddPatientSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient
     * 
     */
    public static void _validateCHPIXAddPatientSpec(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXAddPatientSpec(aClass)){
			executeCons_CHPIXAddPatientSpec_Ch_pix_001_add_NameOrId(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXAddPatientSpec_Ch_pix_001_add_NameOrId(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXAddPatientSpec_Ch_pix_001_add_NameOrId(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_001_add_NameOrId");
		notif.setDescription("At least  Person.name  or  Patient.id  must be non-null.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedadd-CHPIXAddPatientSpec-ch_pix_001_add_NameOrId");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-001"));
		diagnostic.add(notif);
	}

}
