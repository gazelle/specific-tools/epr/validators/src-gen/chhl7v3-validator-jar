package net.ihe.gazelle.hl7v3.validator.chxcpdresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.voc.NullFlavor;	
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHXCPDPatientSpec
  * package :   chxcpdresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02Patient
  * 
  */
public final class CHXCPDPatientSpecValidator{


    private CHXCPDPatientSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_xcpd_007_Mpipid
	* The MPI-PID MUST be returned if there is a match from the EPR-SPID
	*
	*/
	private static boolean _validateCHXCPDPatientSpec_Ch_xcpd_007_Mpipid(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(!!(((String) anElement1.getRoot()) == null) || !anElement1.getRoot().equals("2.16.756.5.30.1.127.3.10.3"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_008_ConfidentialityCode
	* ConfidentialityCode is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPatientSpec_Ch_xcpd_008_ConfidentialityCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getConfidentialityCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_009_VeryImportantPersonCode
	* VeryImportantPersonCode is not an allowed parameter in XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPatientSpec_Ch_xcpd_009_VeryImportantPersonCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
		return (aClass.getVeryImportantPersonCode() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_010_Id
	*  Patient.id  is mandatory.
	*
	*/
	private static boolean _validateCHXCPDPatientSpec_Ch_xcpd_010_Id(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!(anElement1.getNullFlavor() == null)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of class-constraint : CHXCPDPatientSpec
    * Verify if an element of type CHXCPDPatientSpec can be validated by CHXCPDPatientSpec
	*
	*/
	public static boolean _isCHXCPDPatientSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXCPDPatientSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient
     * 
     */
    public static void _validateCHXCPDPatientSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, String location, List<Notification> diagnostic) {
		if (_isCHXCPDPatientSpec(aClass)){
			executeCons_CHXCPDPatientSpec_Ch_xcpd_007_Mpipid(aClass, location, diagnostic);
			executeCons_CHXCPDPatientSpec_Ch_xcpd_008_ConfidentialityCode(aClass, location, diagnostic);
			executeCons_CHXCPDPatientSpec_Ch_xcpd_009_VeryImportantPersonCode(aClass, location, diagnostic);
			executeCons_CHXCPDPatientSpec_Ch_xcpd_010_Id(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXCPDPatientSpec_Ch_xcpd_007_Mpipid(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPatientSpec_Ch_xcpd_007_Mpipid(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_007_Mpipid");
		notif.setDescription("The MPI-PID MUST be returned if there is a match from the EPR-SPID");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_007_Mpipid");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-007"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPatientSpec_Ch_xcpd_008_ConfidentialityCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPatientSpec_Ch_xcpd_008_ConfidentialityCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_008_ConfidentialityCode");
		notif.setDescription("ConfidentialityCode is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_008_ConfidentialityCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-008"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPatientSpec_Ch_xcpd_009_VeryImportantPersonCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPatientSpec_Ch_xcpd_009_VeryImportantPersonCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_009_VeryImportantPersonCode");
		notif.setDescription("VeryImportantPersonCode is not an allowed parameter in XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_009_VeryImportantPersonCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-009"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPatientSpec_Ch_xcpd_010_Id(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPatientSpec_Ch_xcpd_010_Id(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_010_Id");
		notif.setDescription("Patient.id  is mandatory.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPatientSpec-ch_xcpd_010_Id");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-010"));
		diagnostic.add(notif);
	}

}
