package net.ihe.gazelle.hl7v3.validator.chpqdresponsetmp;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;



public class CHPQDRESPONSETMPPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHPQDRESPONSETMPPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient)obj;
			CHPDQPatientSpectmpValidator._validateCHPDQPatientSpectmp(aClass, location, diagnostic);
		}
	
	}

}

