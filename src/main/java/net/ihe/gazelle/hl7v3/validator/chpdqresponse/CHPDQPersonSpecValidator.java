package net.ihe.gazelle.hl7v3.validator.chpdqresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPDQPersonSpec
  * package :   chpdqresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02Person
  * 
  */
public final class CHPDQPersonSpecValidator{


    private CHPDQPersonSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_008_Name
	*  In the  name  field, the birth name is passed with the qualifier BR.
	*
	*/
	private static boolean _validateCHPDQPersonSpec_Ch_pdq_008_Name(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    Boolean result2;
			    result2 = false;
			    
			    /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
			    try{
			    	for (EnFamily anElement2 : anElement1.getFamily()) {
			    	    if ((!(((String) anElement2.getQualifier()) == null) && anElement2.getQualifier().equals("BR"))) {
			    	        result2 = true;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getFamily()) > new Integer(0)) && result2)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of instance by a constraint : ch_pdq_009_ReligiousAffiliationCode
	* ReligiousAffiliationCode is not an allowed parameter in PDQ Response.
	*
	*/
	private static boolean _validateCHPDQPersonSpec_Ch_pdq_009_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getReligiousAffiliationCode() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_pdq_010_RaceCode
	* RaceCode is not an allowed parameter in PDQ Response.
	*
	*/
	private static boolean _validateCHPDQPersonSpec_Ch_pdq_010_RaceCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getRaceCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_pdq_011_EthnicGroupCode
	* EthnicGroupCode is not an allowed parameter in PDQ Response.
	*
	*/
	private static boolean _validateCHPDQPersonSpec_Ch_pdq_011_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getEthnicGroupCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of class-constraint : CHPDQPersonSpec
    * Verify if an element of type CHPDQPersonSpec can be validated by CHPDQPersonSpec
	*
	*/
	public static boolean _isCHPDQPersonSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQPersonSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person
     * 
     */
    public static void _validateCHPDQPersonSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQPersonSpec(aClass)){
			executeCons_CHPDQPersonSpec_Ch_pdq_008_Name(aClass, location, diagnostic);
			executeCons_CHPDQPersonSpec_Ch_pdq_009_ReligiousAffiliationCode(aClass, location, diagnostic);
			executeCons_CHPDQPersonSpec_Ch_pdq_010_RaceCode(aClass, location, diagnostic);
			executeCons_CHPDQPersonSpec_Ch_pdq_011_EthnicGroupCode(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQPersonSpec_Ch_pdq_008_Name(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPersonSpec_Ch_pdq_008_Name(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_pdq_008_Name");
		notif.setDescription("In the  name  field, the birth name is passed with the qualifier BR.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPersonSpec-ch_pdq_008_Name");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-008"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPDQPersonSpec_Ch_pdq_009_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPersonSpec_Ch_pdq_009_ReligiousAffiliationCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_009_ReligiousAffiliationCode");
		notif.setDescription("ReligiousAffiliationCode is not an allowed parameter in PDQ Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPersonSpec-ch_pdq_009_ReligiousAffiliationCode");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-009"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPDQPersonSpec_Ch_pdq_010_RaceCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPersonSpec_Ch_pdq_010_RaceCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_010_RaceCode");
		notif.setDescription("RaceCode is not an allowed parameter in PDQ Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPersonSpec-ch_pdq_010_RaceCode");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-010"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPDQPersonSpec_Ch_pdq_011_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPersonSpec_Ch_pdq_011_EthnicGroupCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_011_EthnicGroupCode");
		notif.setDescription("EthnicGroupCode is not an allowed parameter in PDQ Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPersonSpec-ch_pdq_011_EthnicGroupCode");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-011"));
		diagnostic.add(notif);
	}

}
