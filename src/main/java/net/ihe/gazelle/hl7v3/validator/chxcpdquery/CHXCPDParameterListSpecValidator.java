package net.ihe.gazelle.hl7v3.validator.chxcpdquery;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02LivingSubjectId;



 /**
  * class :        CHXCPDParameterListSpec
  * package :   chxcpdquery
  * Constraint Spec Class
  * class of test : PRPAMT201306UV02ParameterList
  * 
  */
public final class CHXCPDParameterListSpecValidator{


    private CHXCPDParameterListSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectAdministrativeGender
	* LivingSubjectAdministrativeGender MAY be used in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectAdministrativeGender(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectAdministrativeGender()) > new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectBirthPlaceAddress
	* LivingSubjectBirthPlaceAddress is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceAddress(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectBirthPlaceAddress())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectBirthPlaceName
	* LivingSubjectBirthPlaceName is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceName(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectBirthPlaceName())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectBirthTime
	* LivingSubjectBirthTime MAY be used in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthTime(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectBirthTime()) > new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectDeceasedTime
	* LivingSubjectDeceasedTime is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectDeceasedTime(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectDeceasedTime())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectId
	* LivingSubjectId is mandatory in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectId()) > new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_LivingSubjectName
	* LivingSubjectName MAY be used in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectName(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectName()) > new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_MothersMaidenName
	* MothersMaidenName is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_MothersMaidenName(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getMothersMaidenName())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_OtherIDsScopingOrganization
	* OtherIDsScopingOrganization is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_OtherIDsScopingOrganization(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getOtherIDsScopingOrganization())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_PatientAddress
	* PatientAddress is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_PatientAddress(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientAddress())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_PatientStatusCode
	* PatientStatusCode is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_PatientStatusCode(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientStatusCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_PatientTelecom
	* PatientTelecom is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_PatientTelecom(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientTelecom())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_PrincipalCareProviderId
	* PrincipalCareProviderId is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProviderId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPrincipalCareProviderId())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_004_PrincipalCareProvisionId
	* PrincipalCareProvisionId is not an allowed parameter in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProvisionId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPrincipalCareProvisionId())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_005_LivingSubjectId
	* The LivingSubjectId Parameter MUST contain the EPR-PID in XCPD Query.
	*
	*/
	private static boolean _validateCHXCPDParameterListSpec_Ch_xcpd_005_LivingSubjectId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
		java.util.ArrayList<II> result1;
		result1 = new java.util.ArrayList<II>();
		
		/* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
		for (PRPAMT201306UV02LivingSubjectId anElement1 : aClass.getLivingSubjectId()) {
		    result1.addAll(anElement1.getValue());
		}
		
		java.util.ArrayList<II> result3;
		result3 = new java.util.ArrayList<II>();
		
		/* Iterator Collect: Iterate through all elements and collect them. Elements which are collections are flattened. */
		for (PRPAMT201306UV02LivingSubjectId anElement2 : aClass.getLivingSubjectId()) {
		    result3.addAll(anElement2.getValue());
		}
		Boolean result2;
		result2 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement3 : result3) {
			    if (!((((!(((String) anElement3.getExtension()) == null) && ((Object) anElement3.getExtension().length()).equals(new Integer(18))) && anElement3.getExtension().substring(new Integer(1) - 1, new Integer(8)).equals("76133761")) && !(((String) anElement3.getRoot()) == null)) && anElement3.getRoot().equals("2.16.756.5.30.1.127.3.10.3"))) {
			        result2 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLivingSubjectId()) > new Integer(0)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0))) && result2);
		
		
	}

	/**
	* Validation of class-constraint : CHXCPDParameterListSpec
    * Verify if an element of type CHXCPDParameterListSpec can be validated by CHXCPDParameterListSpec
	*
	*/
	public static boolean _isCHXCPDParameterListSpec(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXCPDParameterListSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList
     * 
     */
    public static void _validateCHXCPDParameterListSpec(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, String location, List<Notification> diagnostic) {
		if (_isCHXCPDParameterListSpec(aClass)){
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectAdministrativeGender(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceAddress(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceName(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthTime(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectDeceasedTime(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectId(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectName(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_MothersMaidenName(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_OtherIDsScopingOrganization(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PatientAddress(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PatientStatusCode(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PatientTelecom(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProviderId(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProvisionId(aClass, location, diagnostic);
			executeCons_CHXCPDParameterListSpec_Ch_xcpd_005_LivingSubjectId(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectAdministrativeGender(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectAdministrativeGender(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectAdministrativeGender");
		notif.setDescription("LivingSubjectAdministrativeGender MAY be used in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectAdministrativeGender");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceAddress(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceAddress(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectBirthPlaceAddress");
		notif.setDescription("LivingSubjectBirthPlaceAddress is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthPlaceAddress");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceName(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthPlaceName(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectBirthPlaceName");
		notif.setDescription("LivingSubjectBirthPlaceName is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthPlaceName");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthTime(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectBirthTime(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectBirthTime");
		notif.setDescription("LivingSubjectBirthTime MAY be used in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectBirthTime");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectDeceasedTime(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectDeceasedTime(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectDeceasedTime");
		notif.setDescription("LivingSubjectDeceasedTime is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectDeceasedTime");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectId(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectId");
		notif.setDescription("LivingSubjectId is mandatory in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectId");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectName(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_LivingSubjectName(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_xcpd_004_LivingSubjectName");
		notif.setDescription("LivingSubjectName MAY be used in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_LivingSubjectName");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_MothersMaidenName(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_MothersMaidenName(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_MothersMaidenName");
		notif.setDescription("MothersMaidenName is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_MothersMaidenName");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_OtherIDsScopingOrganization(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_OtherIDsScopingOrganization(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_OtherIDsScopingOrganization");
		notif.setDescription("OtherIDsScopingOrganization is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_OtherIDsScopingOrganization");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PatientAddress(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_PatientAddress(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_PatientAddress");
		notif.setDescription("PatientAddress is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientAddress");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PatientStatusCode(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_PatientStatusCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_PatientStatusCode");
		notif.setDescription("PatientStatusCode is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientStatusCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PatientTelecom(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_PatientTelecom(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_PatientTelecom");
		notif.setDescription("PatientTelecom is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PatientTelecom");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProviderId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProviderId(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_PrincipalCareProviderId");
		notif.setDescription("PrincipalCareProviderId is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PrincipalCareProviderId");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProvisionId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_004_PrincipalCareProvisionId(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_004_PrincipalCareProvisionId");
		notif.setDescription("PrincipalCareProvisionId is not an allowed parameter in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_004_PrincipalCareProvisionId");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDParameterListSpec_Ch_xcpd_005_LivingSubjectId(net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDParameterListSpec_Ch_xcpd_005_LivingSubjectId(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("ch_xcpd_005_LivingSubjectId");
		notif.setDescription("The LivingSubjectId Parameter MUST contain the EPR-PID in XCPD Query.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdquery-CHXCPDParameterListSpec-ch_xcpd_005_LivingSubjectId");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-005"));
		diagnostic.add(notif);
	}

}
