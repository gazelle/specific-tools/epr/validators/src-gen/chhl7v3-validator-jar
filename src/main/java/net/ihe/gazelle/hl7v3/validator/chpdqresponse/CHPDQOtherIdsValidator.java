package net.ihe.gazelle.hl7v3.validator.chpdqresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPDQOtherIds
  * package :   chpdqresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02OtherIDs
  * 
  */
public final class CHPDQOtherIdsValidator{


    private CHPDQOtherIdsValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_012_EPRPID
	* In the  OtherIDs  parameter, the EPR-PID MAY be added here
	*
	*/
	private static boolean _validateCHPDQOtherIds_Ch_pdq_012_EPRPID(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (anElement1.getRoot().equals("2.16.756.5.30.1.127.3.10.3")) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of class-constraint : CHPDQOtherIds
    * Verify if an element of type CHPDQOtherIds can be validated by CHPDQOtherIds
	*
	*/
	public static boolean _isCHPDQOtherIds(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQOtherIds
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs
     * 
     */
    public static void _validateCHPDQOtherIds(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQOtherIds(aClass)){
			executeCons_CHPDQOtherIds_Ch_pdq_012_EPRPID(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQOtherIds_Ch_pdq_012_EPRPID(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQOtherIds_Ch_pdq_012_EPRPID(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_pdq_012_EPRPID");
		notif.setDescription("In the  OtherIDs  parameter, the EPR-PID MAY be added here");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQOtherIds-ch_pdq_012_EPRPID");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-012"));
		diagnostic.add(notif);
	}

}
