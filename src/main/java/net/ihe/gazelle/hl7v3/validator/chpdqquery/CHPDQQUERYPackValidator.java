package net.ihe.gazelle.hl7v3.validator.chpdqquery;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;





public class CHPDQQUERYPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHPDQQUERYPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList){
			net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList aClass = ( net.ihe.gazelle.hl7v3.prpamt201306UV02.PRPAMT201306UV02ParameterList)obj;
			CHPDQParameterListSpecValidator._validateCHPDQParameterListSpec(aClass, location, diagnostic);
		}
	
	}

}

