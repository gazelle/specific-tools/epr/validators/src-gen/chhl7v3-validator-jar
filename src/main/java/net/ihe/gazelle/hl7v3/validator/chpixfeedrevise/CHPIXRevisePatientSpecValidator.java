package net.ihe.gazelle.hl7v3.validator.chpixfeedrevise;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDsId;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientId;



 /**
  * class :        CHPIXRevisePatientSpec
  * package :   chpixfeedrevise
  * Constraint Spec Class
  * class of test : PRPAMT201302UV02Patient
  * 
  */
public final class CHPIXRevisePatientSpecValidator{


    private CHPIXRevisePatientSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_001_revise_NameOrId
	* At least  Person.name  or  Patient.id  must be non-null.
	*
	*/
	private static boolean _validateCHPIXRevisePatientSpec_Ch_pix_001_revise_NameOrId(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PRPAMT201302UV02PatientId anElement1 : aClass.getId()) {
			    if (!!(anElement1.getNullFlavor() == null)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		Boolean result2;
		result2 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement2 : aClass.getPatientPerson().getName()) {
			    if (!!(anElement2.getNullFlavor() == null)) {
			        result2 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientPerson().getName()) > new Integer(0))) && !(result1 && result2));
		
		
	}

	/**
	* Validation of class-constraint : CHPIXRevisePatientSpec
    * Verify if an element of type CHPIXRevisePatientSpec can be validated by CHPIXRevisePatientSpec
	*
	*/
	public static boolean _isCHPIXRevisePatientSpec(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXRevisePatientSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient
     * 
     */
    public static void _validateCHPIXRevisePatientSpec(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXRevisePatientSpec(aClass)){
			executeCons_CHPIXRevisePatientSpec_Ch_pix_001_revise_NameOrId(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXRevisePatientSpec_Ch_pix_001_revise_NameOrId(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXRevisePatientSpec_Ch_pix_001_revise_NameOrId(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_001_revise_NameOrId");
		notif.setDescription("At least  Person.name  or  Patient.id  must be non-null.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedrevise-CHPIXRevisePatientSpec-ch_pix_001_revise_NameOrId");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-001"));
		diagnostic.add(notif);
	}

}
