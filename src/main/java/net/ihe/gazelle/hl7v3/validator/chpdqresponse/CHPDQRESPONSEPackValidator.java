package net.ihe.gazelle.hl7v3.validator.chpdqresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.datatypes.PN;



public class CHPDQRESPONSEPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHPDQRESPONSEPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess){
			net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess aClass = ( net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess)obj;
			CHPDQControlActProcessValidator._validateCHPDQControlActProcess(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02OtherIDs)obj;
			CHPDQOtherIdsValidator._validateCHPDQOtherIds(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient)obj;
			CHPDQPatientSpecValidator._validateCHPDQPatientSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person)obj;
			CHPDQPersonSpecValidator._validateCHPDQPersonSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship)obj;
			CHPDQPersonalRelationshipValidator._validateCHPDQPersonalRelationship(aClass, location, diagnostic);
		}
	
	}

}

