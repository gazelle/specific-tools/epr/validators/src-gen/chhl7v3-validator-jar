package net.ihe.gazelle.hl7v3.validator.chpixfeedrevise;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDsId;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientId;



 /**
  * class :        CHPIXRevisePersonSpec
  * package :   chpixfeedrevise
  * Constraint Spec Class
  * class of test : PRPAMT201302UV02Person
  * 
  */
public final class CHPIXRevisePersonSpecValidator{


    private CHPIXRevisePersonSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_002_revise_Name
	* In the  name  field, the birth name is passed with the qualifier BR.
	*
	*/
	private static boolean _validateCHPIXRevisePersonSpec_Ch_pix_002_revise_Name(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    Boolean result2;
			    result2 = false;
			    
			    /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
			    try{
			    	for (EnFamily anElement2 : anElement1.getFamily()) {
			    	    if ((!(((String) anElement2.getQualifier()) == null) && anElement2.getQualifier().equals("BR"))) {
			    	        result2 = true;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getFamily()) > new Integer(0)) && result2)) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of instance by a constraint : ch_pix_003_revise_ReligiousAffiliationCode
	* ReligiousAffiliationCode is not an allowed parameter.
	*
	*/
	private static boolean _validateCHPIXRevisePersonSpec_Ch_pix_003_revise_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass){
		return (aClass.getReligiousAffiliationCode() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_pix_004_revise_RaceCode
	* RaceCode is not an allowed parameter.
	*
	*/
	private static boolean _validateCHPIXRevisePersonSpec_Ch_pix_004_revise_RaceCode(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getRaceCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_pix_005_revise_EthnicGroupCode
	* EthnicGroupCode is not an allowed parameter.
	*
	*/
	private static boolean _validateCHPIXRevisePersonSpec_Ch_pix_005_revise_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getEthnicGroupCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of class-constraint : CHPIXRevisePersonSpec
    * Verify if an element of type CHPIXRevisePersonSpec can be validated by CHPIXRevisePersonSpec
	*
	*/
	public static boolean _isCHPIXRevisePersonSpec(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXRevisePersonSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person
     * 
     */
    public static void _validateCHPIXRevisePersonSpec(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXRevisePersonSpec(aClass)){
			executeCons_CHPIXRevisePersonSpec_Ch_pix_002_revise_Name(aClass, location, diagnostic);
			executeCons_CHPIXRevisePersonSpec_Ch_pix_003_revise_ReligiousAffiliationCode(aClass, location, diagnostic);
			executeCons_CHPIXRevisePersonSpec_Ch_pix_004_revise_RaceCode(aClass, location, diagnostic);
			executeCons_CHPIXRevisePersonSpec_Ch_pix_005_revise_EthnicGroupCode(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXRevisePersonSpec_Ch_pix_002_revise_Name(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXRevisePersonSpec_Ch_pix_002_revise_Name(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_pix_002_revise_Name");
		notif.setDescription("In the  name  field, the birth name is passed with the qualifier BR.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_002_revise_Name");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-002"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPIXRevisePersonSpec_Ch_pix_003_revise_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXRevisePersonSpec_Ch_pix_003_revise_ReligiousAffiliationCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_003_revise_ReligiousAffiliationCode");
		notif.setDescription("ReligiousAffiliationCode is not an allowed parameter.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_003_revise_ReligiousAffiliationCode");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-003"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPIXRevisePersonSpec_Ch_pix_004_revise_RaceCode(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXRevisePersonSpec_Ch_pix_004_revise_RaceCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_004_revise_RaceCode");
		notif.setDescription("RaceCode is not an allowed parameter.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_004_revise_RaceCode");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPIXRevisePersonSpec_Ch_pix_005_revise_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXRevisePersonSpec_Ch_pix_005_revise_EthnicGroupCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_005_revise_EthnicGroupCode");
		notif.setDescription("EthnicGroupCode is not an allowed parameter.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedrevise-CHPIXRevisePersonSpec-ch_pix_005_revise_EthnicGroupCode");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-005"));
		diagnostic.add(notif);
	}

}
