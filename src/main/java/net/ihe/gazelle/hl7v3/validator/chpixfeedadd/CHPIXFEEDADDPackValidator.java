package net.ihe.gazelle.hl7v3.validator.chpixfeedadd;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.PN;



public class CHPIXFEEDADDPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHPIXFEEDADDPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient){
			net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient aClass = ( net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Patient)obj;
			CHPIXAddPatientSpecValidator._validateCHPIXAddPatientSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person){
			net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass = ( net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person)obj;
			CHPIXAddPersonSpecValidator._validateCHPIXAddPersonSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship){
			net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship aClass = ( net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship)obj;
			CHPIXAddPersonalRelationshipSpecValidator._validateCHPIXAddPersonalRelationshipSpec(aClass, location, diagnostic);
		}
	
	}

}

