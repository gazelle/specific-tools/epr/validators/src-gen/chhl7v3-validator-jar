package net.ihe.gazelle.hl7v3.validator.chpdqresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPDQPatientSpec
  * package :   chpdqresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02Patient
  * 
  */
public final class CHPDQPatientSpecValidator{


    private CHPDQPatientSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_007_NameOrId
	* Either  Person.name  or  Patient.id  must be non-null.
	*
	*/
	private static boolean _validateCHPDQPatientSpec_Ch_pdq_007_NameOrId(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!!(anElement1.getNullFlavor() == null)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		Boolean result2;
		result2 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement2 : aClass.getPatientPerson().getName()) {
			    if (!!(anElement2.getNullFlavor() == null)) {
			        result2 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPatientPerson().getName()) > new Integer(0))) && !(result1 && result2));
		
		
	}

	/**
	* Validation of class-constraint : CHPDQPatientSpec
    * Verify if an element of type CHPDQPatientSpec can be validated by CHPDQPatientSpec
	*
	*/
	public static boolean _isCHPDQPatientSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQPatientSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient
     * 
     */
    public static void _validateCHPDQPatientSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQPatientSpec(aClass)){
			executeCons_CHPDQPatientSpec_Ch_pdq_007_NameOrId(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQPatientSpec_Ch_pdq_007_NameOrId(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPatientSpec_Ch_pdq_007_NameOrId(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_007_NameOrId");
		notif.setDescription("Either  Person.name  or  Patient.id  must be non-null.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPatientSpec-ch_pdq_007_NameOrId");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-007"));
		diagnostic.add(notif);
	}

}
