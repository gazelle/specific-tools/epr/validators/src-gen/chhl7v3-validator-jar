package net.ihe.gazelle.hl7v3.validator.chxcpdresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.voc.NullFlavor;	
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHXCPDPersonSpec
  * package :   chxcpdresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02Person
  * 
  */
public final class CHXCPDPersonSpecValidator{


    private CHXCPDPersonSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_xcpd_011_Telecom
	* Telecom is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_011_Telecom(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getTelecom())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_014_DeceasedInd
	* DeceasedInd is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_014_DeceasedInd(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getDeceasedInd() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_015_DeceasedTime
	* DeceasedTime is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_015_DeceasedTime(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getDeceasedTime() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_016_MultipleBirthInd
	* MultipleBirthInd is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_016_MultipleBirthInd(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getMultipleBirthInd() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_017_MultipleBirthOrderNumber
	* MultipleBirthOrderNumber is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_017_MultipleBirthOrderNumber(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getMultipleBirthOrderNumber() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_018_Addr
	* Addr is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_018_Addr(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAddr())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_019_MaritalStatusCode
	* MaritalStatusCodeCode is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_019_MaritalStatusCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getMaritalStatusCode() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_020_ReligiousAffiliationCode
	* ReligiousAffiliationCode is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_020_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return (aClass.getReligiousAffiliationCode() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_021_RaceCode
	* RaceCode is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_021_RaceCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getRaceCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_022_EthnicGroupCode
	* EthnicGroupCode is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_022_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getEthnicGroupCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_023_AsOtherIDs
	* AsOtherIDs is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_023_AsOtherIDs(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAsOtherIDs())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_025_PersonalRelationship
	* PersonalRelationship is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_025_PersonalRelationship(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getPersonalRelationship())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_028_AsCitizen
	* AsCitizen is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_028_AsCitizen(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAsCitizen())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_034_AsEmployee
	* AsEmployee is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_034_AsEmployee(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getAsEmployee())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_037_LanguageCommunication
	* LanguageCommunication is not an allowed parameter in  XCPD Response.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_037_LanguageCommunication(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getLanguageCommunication())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_xcpd_040_Name
	* Name(s) for this person. May be null i.e., <name nullFlavor=”NA”/> only if the request contained only a patient identifier and no demographic data.
	*
	*/
	private static boolean _validateCHXCPDPersonSpec_Ch_xcpd_040_Name(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    if (!(!(!(anElement1.getNullFlavor() == null) && anElement1.getNullFlavor().equals(NullFlavor.NA)) || ((aClass.getAdministrativeGenderCode() == null) && (aClass.getBirthTime() == null)))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of class-constraint : CHXCPDPersonSpec
    * Verify if an element of type CHXCPDPersonSpec can be validated by CHXCPDPersonSpec
	*
	*/
	public static boolean _isCHXCPDPersonSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXCPDPersonSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person
     * 
     */
    public static void _validateCHXCPDPersonSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, String location, List<Notification> diagnostic) {
		if (_isCHXCPDPersonSpec(aClass)){
			executeCons_CHXCPDPersonSpec_Ch_xcpd_011_Telecom(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_014_DeceasedInd(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_015_DeceasedTime(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_016_MultipleBirthInd(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_017_MultipleBirthOrderNumber(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_018_Addr(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_019_MaritalStatusCode(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_020_ReligiousAffiliationCode(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_021_RaceCode(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_022_EthnicGroupCode(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_023_AsOtherIDs(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_025_PersonalRelationship(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_028_AsCitizen(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_034_AsEmployee(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_037_LanguageCommunication(aClass, location, diagnostic);
			executeCons_CHXCPDPersonSpec_Ch_xcpd_040_Name(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_011_Telecom(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_011_Telecom(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_011_Telecom");
		notif.setDescription("Telecom is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_011_Telecom");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-011"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_014_DeceasedInd(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_014_DeceasedInd(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_014_DeceasedInd");
		notif.setDescription("DeceasedInd is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_014_DeceasedInd");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-014"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_015_DeceasedTime(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_015_DeceasedTime(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_015_DeceasedTime");
		notif.setDescription("DeceasedTime is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_015_DeceasedTime");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-015"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_016_MultipleBirthInd(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_016_MultipleBirthInd(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_016_MultipleBirthInd");
		notif.setDescription("MultipleBirthInd is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_016_MultipleBirthInd");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-016"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_017_MultipleBirthOrderNumber(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_017_MultipleBirthOrderNumber(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_017_MultipleBirthOrderNumber");
		notif.setDescription("MultipleBirthOrderNumber is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_017_MultipleBirthOrderNumber");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-017"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_018_Addr(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_018_Addr(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_018_Addr");
		notif.setDescription("Addr is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_018_Addr");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-018"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_019_MaritalStatusCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_019_MaritalStatusCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_019_MaritalStatusCode");
		notif.setDescription("MaritalStatusCodeCode is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_019_MaritalStatusCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-019"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_020_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_020_ReligiousAffiliationCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_020_ReligiousAffiliationCode");
		notif.setDescription("ReligiousAffiliationCode is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_020_ReligiousAffiliationCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-020"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_021_RaceCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_021_RaceCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_021_RaceCode");
		notif.setDescription("RaceCode is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_021_RaceCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-021"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_022_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_022_EthnicGroupCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_022_EthnicGroupCode");
		notif.setDescription("EthnicGroupCode is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_022_EthnicGroupCode");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-022"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_023_AsOtherIDs(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_023_AsOtherIDs(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_023_AsOtherIDs");
		notif.setDescription("AsOtherIDs is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_023_AsOtherIDs");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-023"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_025_PersonalRelationship(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_025_PersonalRelationship(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_025_PersonalRelationship");
		notif.setDescription("PersonalRelationship is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_025_PersonalRelationship");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-025"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_028_AsCitizen(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_028_AsCitizen(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_028_AsCitizen");
		notif.setDescription("AsCitizen is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_028_AsCitizen");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-028"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_034_AsEmployee(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_034_AsEmployee(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_034_AsEmployee");
		notif.setDescription("AsEmployee is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_034_AsEmployee");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-034"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_037_LanguageCommunication(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_037_LanguageCommunication(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_037_LanguageCommunication");
		notif.setDescription("LanguageCommunication is not an allowed parameter in  XCPD Response.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_037_LanguageCommunication");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-037"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHXCPDPersonSpec_Ch_xcpd_040_Name(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDPersonSpec_Ch_xcpd_040_Name(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_040_Name");
		notif.setDescription("Name(s) for this person. May be null i.e., <name nullFlavor=”NA”/> only if the request contained only a patient identifier and no demographic data.");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDPersonSpec-ch_xcpd_040_Name");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-040"));
		diagnostic.add(notif);
	}

}
