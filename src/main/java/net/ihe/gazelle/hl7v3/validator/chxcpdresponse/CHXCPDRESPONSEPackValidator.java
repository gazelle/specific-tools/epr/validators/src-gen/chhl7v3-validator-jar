package net.ihe.gazelle.hl7v3.validator.chxcpdresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.voc.NullFlavor;	
import net.ihe.gazelle.hl7v3.datatypes.PN;



public class CHXCPDRESPONSEPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHXCPDRESPONSEPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient)obj;
			CHXCPDPatientSpecValidator._validateCHXCPDPatientSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Person)obj;
			CHXCPDPersonSpecValidator._validateCHXCPDPersonSpec(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation){
			net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation aClass = ( net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation)obj;
			CHXCPDQueryMatchObservationSpecValidator._validateCHXCPDQueryMatchObservationSpec(aClass, location, diagnostic);
		}
	
	}

}

