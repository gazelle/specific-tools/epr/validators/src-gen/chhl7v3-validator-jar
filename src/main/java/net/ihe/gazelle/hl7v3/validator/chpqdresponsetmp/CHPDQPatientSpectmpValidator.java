package net.ihe.gazelle.hl7v3.validator.chpqdresponsetmp;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;



 /**
  * class :        CHPDQPatientSpectmp
  * package :   chpqdresponsetmp
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02Patient
  * 
  */
public final class CHPDQPatientSpectmpValidator{


    private CHPDQPatientSpectmpValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_006_Id
	* The extension attribut of ID should be valued with the patient EPR-PID
	*
	*/
	private static boolean _validateCHPDQPatientSpectmp_Ch_pdq_006_Id(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (II anElement1 : aClass.getId()) {
			    if (!((!(((String) anElement1.getExtension()) == null) && ((Object) anElement1.getExtension().length()).equals(new Integer(18))) && anElement1.getExtension().substring(new Integer(1) - 1, new Integer(8)).equals("76133761"))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (!(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getId()) > new Integer(0)) || result1);
		
		
	}

	/**
	* Validation of class-constraint : CHPDQPatientSpectmp
    * Verify if an element of type CHPDQPatientSpectmp can be validated by CHPDQPatientSpectmp
	*
	*/
	public static boolean _isCHPDQPatientSpectmp(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQPatientSpectmp
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient
     * 
     */
    public static void _validateCHPDQPatientSpectmp(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQPatientSpectmp(aClass)){
			executeCons_CHPDQPatientSpectmp_Ch_pdq_006_Id(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQPatientSpectmp_Ch_pdq_006_Id(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02Patient aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPatientSpectmp_Ch_pdq_006_Id(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_006_Id");
		notif.setDescription("The extension attribut of ID should be valued with the patient EPR-PID");
		notif.setLocation(location);
		notif.setIdentifiant("chpqdresponsetmp-CHPDQPatientSpectmp-ch_pdq_006_Id");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-006"));
		diagnostic.add(notif);
	}

}
