package net.ihe.gazelle.hl7v3.validator.chpixfeedrevise;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.PN;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDs;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02OtherIDsId;
import net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PatientId;



 /**
  * class :        CHPIXRevisePersonalRelationshipSpec
  * package :   chpixfeedrevise
  * Constraint Spec Class
  * class of test : PRPAMT201302UV02PersonalRelationship
  * 
  */
public final class CHPIXRevisePersonalRelationshipSpecValidator{


    private CHPIXRevisePersonalRelationshipSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_007_revise_Code
	* The code MUST be  FTH  for the Father and  MTH  for the Mother
	*
	*/
	private static boolean _validateCHPIXRevisePersonalRelationshipSpec_Ch_pix_007_revise_Code(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship aClass){
		return (((!(aClass.getCode() == null) && !(((String) aClass.getCode().getCode()) == null)) && (!aClass.getCode().getDisplayName().equals("Mother") || aClass.getCode().getCode().equals("MTH"))) && (!aClass.getCode().getDisplayName().equals("Father") || aClass.getCode().getCode().equals("FTH")));
		
	}

	/**
	* Validation of class-constraint : CHPIXRevisePersonalRelationshipSpec
    * Verify if an element of type CHPIXRevisePersonalRelationshipSpec can be validated by CHPIXRevisePersonalRelationshipSpec
	*
	*/
	public static boolean _isCHPIXRevisePersonalRelationshipSpec(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXRevisePersonalRelationshipSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship
     * 
     */
    public static void _validateCHPIXRevisePersonalRelationshipSpec(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXRevisePersonalRelationshipSpec(aClass)){
			executeCons_CHPIXRevisePersonalRelationshipSpec_Ch_pix_007_revise_Code(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXRevisePersonalRelationshipSpec_Ch_pix_007_revise_Code(net.ihe.gazelle.hl7v3.prpamt201302UV02.PRPAMT201302UV02PersonalRelationship aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXRevisePersonalRelationshipSpec_Ch_pix_007_revise_Code(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_007_revise_Code");
		notif.setDescription("The code MUST be  FTH  for the Father and  MTH  for the Mother");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedrevise-CHPIXRevisePersonalRelationshipSpec-ch_pix_007_revise_Code");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-007"));
		diagnostic.add(notif);
	}

}
