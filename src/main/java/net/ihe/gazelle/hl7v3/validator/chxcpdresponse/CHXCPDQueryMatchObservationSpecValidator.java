package net.ihe.gazelle.hl7v3.validator.chxcpdresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.INT;
import net.ihe.gazelle.hl7v3.voc.NullFlavor;	
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHXCPDQueryMatchObservationSpec
  * package :   chxcpdresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02QueryMatchObservation
  * 
  */
public final class CHXCPDQueryMatchObservationSpecValidator{


    private CHXCPDQueryMatchObservationSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_xcpd_039_QueryMatchObservation
	* In the  QueryMatchObservation  Node, the  value  field must be a numeric value between 0 (excluded) and 100 (0 < percentage value <= 100) MUST be used (100 for an exact match).
	*
	*/
	private static boolean _validateCHXCPDQueryMatchObservationSpec_Ch_xcpd_039_QueryMatchObservation(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation aClass){
		return ((!(aClass.getValue() == null) && !(((Integer) ((INT) aClass.getValue()).getValue()) == null)) && ((((INT) aClass.getValue()).getValue() <= new Integer(100)) && (((INT) aClass.getValue()).getValue() > new Integer(0))));
		
	}

	/**
	* Validation of class-constraint : CHXCPDQueryMatchObservationSpec
    * Verify if an element of type CHXCPDQueryMatchObservationSpec can be validated by CHXCPDQueryMatchObservationSpec
	*
	*/
	public static boolean _isCHXCPDQueryMatchObservationSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXCPDQueryMatchObservationSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation
     * 
     */
    public static void _validateCHXCPDQueryMatchObservationSpec(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation aClass, String location, List<Notification> diagnostic) {
		if (_isCHXCPDQueryMatchObservationSpec(aClass)){
			executeCons_CHXCPDQueryMatchObservationSpec_Ch_xcpd_039_QueryMatchObservation(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXCPDQueryMatchObservationSpec_Ch_xcpd_039_QueryMatchObservation(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02QueryMatchObservation aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXCPDQueryMatchObservationSpec_Ch_xcpd_039_QueryMatchObservation(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_xcpd_039_QueryMatchObservation");
		notif.setDescription("In the  QueryMatchObservation  Node, the  value  field must be a numeric value between 0 (excluded) and 100 (0 < percentage value <= 100) MUST be used (100 for an exact match).");
		notif.setLocation(location);
		notif.setIdentifiant("chxcpdresponse-CHXCPDQueryMatchObservationSpec-ch_xcpd_039_QueryMatchObservation");
		notif.getAssertions().add(new Assertion("CH-XCPD","CH-XCPD-039"));
		diagnostic.add(notif);
	}

}
