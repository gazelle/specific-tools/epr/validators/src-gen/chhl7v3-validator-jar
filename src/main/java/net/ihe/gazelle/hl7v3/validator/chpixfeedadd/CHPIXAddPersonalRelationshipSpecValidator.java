package net.ihe.gazelle.hl7v3.validator.chpixfeedadd;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPIXAddPersonalRelationshipSpec
  * package :   chpixfeedadd
  * Constraint Spec Class
  * class of test : PRPAMT201301UV02PersonalRelationship
  * 
  */
public final class CHPIXAddPersonalRelationshipSpecValidator{


    private CHPIXAddPersonalRelationshipSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_007_add_Code
	* The code MUST be  FTH  for the Father and  MTH  for the Mother
	*
	*/
	private static boolean _validateCHPIXAddPersonalRelationshipSpec_Ch_pix_007_add_Code(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship aClass){
		return (((!(aClass.getCode() == null) && !(((String) aClass.getCode().getCode()) == null)) && (!aClass.getCode().getDisplayName().equals("Mother") || aClass.getCode().getCode().equals("MTH"))) && (!aClass.getCode().getDisplayName().equals("Father") || aClass.getCode().getCode().equals("FTH")));
		
	}

	/**
	* Validation of class-constraint : CHPIXAddPersonalRelationshipSpec
    * Verify if an element of type CHPIXAddPersonalRelationshipSpec can be validated by CHPIXAddPersonalRelationshipSpec
	*
	*/
	public static boolean _isCHPIXAddPersonalRelationshipSpec(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXAddPersonalRelationshipSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship
     * 
     */
    public static void _validateCHPIXAddPersonalRelationshipSpec(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXAddPersonalRelationshipSpec(aClass)){
			executeCons_CHPIXAddPersonalRelationshipSpec_Ch_pix_007_add_Code(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXAddPersonalRelationshipSpec_Ch_pix_007_add_Code(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02PersonalRelationship aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXAddPersonalRelationshipSpec_Ch_pix_007_add_Code(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_007_add_Code");
		notif.setDescription("The code MUST be  FTH  for the Father and  MTH  for the Mother");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedadd-CHPIXAddPersonalRelationshipSpec-ch_pix_007_add_Code");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-007"));
		diagnostic.add(notif);
	}

}
