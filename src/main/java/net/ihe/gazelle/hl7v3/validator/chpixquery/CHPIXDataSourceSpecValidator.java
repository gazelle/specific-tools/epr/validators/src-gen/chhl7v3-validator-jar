package net.ihe.gazelle.hl7v3.validator.chpixquery;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02DataSource;



 /**
  * class :        CHPIXDataSourceSpec
  * package :   chpixquery
  * Constraint Spec Class
  * class of test : PRPAMT201307UV02ParameterList
  * 
  */
public final class CHPIXDataSourceSpecValidator{


    private CHPIXDataSourceSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_008_MpipidOrEprSpid
	* The  DataSource  Parameter MUST be specified to the assigning authority/authorities of the MPI-PID and the EPR-SPID in the affinity domain. See also ITI TF-2b, chapter 3.45.4.1.2.1
	*
	*/
	private static boolean _validateCHPIXDataSourceSpec_Ch_pix_008_MpipidOrEprSpid(net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList aClass){
		java.util.ArrayList<PRPAMT201307UV02DataSource> result1;
		result1 = new java.util.ArrayList<PRPAMT201307UV02DataSource>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (PRPAMT201307UV02DataSource anElement1 : aClass.getDataSource()) {
			    Boolean result2;
			    result2 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (II anElement2 : anElement1.getValue()) {
			    	    if (!(!(((String) anElement2.getRoot()) == null) && anElement2.getRoot().equals("2.16.756.5.30.1.127.3.10.3"))) {
			    	        result2 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if ((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getValue())).equals(new Integer(1)) && result2)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1)).equals(new Integer(1));
		
		
	}

	/**
	* Validation of class-constraint : CHPIXDataSourceSpec
    * Verify if an element of type CHPIXDataSourceSpec can be validated by CHPIXDataSourceSpec
	*
	*/
	public static boolean _isCHPIXDataSourceSpec(net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXDataSourceSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList
     * 
     */
    public static void _validateCHPIXDataSourceSpec(net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXDataSourceSpec(aClass)){
			executeCons_CHPIXDataSourceSpec_Ch_pix_008_MpipidOrEprSpid(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXDataSourceSpec_Ch_pix_008_MpipidOrEprSpid(net.ihe.gazelle.hl7v3.prpamt201307UV02.PRPAMT201307UV02ParameterList aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXDataSourceSpec_Ch_pix_008_MpipidOrEprSpid(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_008_MpipidOrEprSpid");
		notif.setDescription("The  DataSource  Parameter MUST be specified to the assigning authority/authorities of the MPI-PID and the EPR-SPID in the affinity domain. See also ITI TF-2b, chapter 3.45.4.1.2.1");
		notif.setLocation(location);
		notif.setIdentifiant("chpixquery-CHPIXDataSourceSpec-ch_pix_008_MpipidOrEprSpid");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-008"));
		diagnostic.add(notif);
	}

}
