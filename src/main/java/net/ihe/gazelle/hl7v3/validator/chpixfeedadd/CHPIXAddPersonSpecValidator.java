package net.ihe.gazelle.hl7v3.validator.chpixfeedadd;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPIXAddPersonSpec
  * package :   chpixfeedadd
  * Constraint Spec Class
  * class of test : PRPAMT201301UV02Person
  * 
  */
public final class CHPIXAddPersonSpecValidator{


    private CHPIXAddPersonSpecValidator() {}



	/**
	* Validation of instance by a constraint : ch_pix_002_add_Name
	*  In the  name  field, the birth name is passed with the qualifier BR.
	*
	*/
	private static boolean _validateCHPIXAddPersonSpec_Ch_pix_002_add_Name(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass){
		Boolean result1;
		result1 = false;
		
		/* Iterator Exists: Iterate and check, if any element fulfills the condition. */
		try{
			for (PN anElement1 : aClass.getName()) {
			    Boolean result2;
			    result2 = false;
			    
			    /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
			    try{
			    	for (EnFamily anElement2 : anElement1.getFamily()) {
			    	    if ((!(((String) anElement2.getQualifier()) == null) && anElement2.getQualifier().equals("BR"))) {
			    	        result2 = true;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getFamily()) > new Integer(0)) && result2)) {
			        result1 = true;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName()) > new Integer(0)) && result1);
		
		
	}

	/**
	* Validation of instance by a constraint : ch_pix_003_add_ReligiousAffiliationCode
	* ReligiousAffiliationCode is not an allowed parameter.
	*
	*/
	private static boolean _validateCHPIXAddPersonSpec_Ch_pix_003_add_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass){
		return (aClass.getReligiousAffiliationCode() == null);
		
	}

	/**
	* Validation of instance by a constraint : ch_pix_004_add_RaceCode
	* RaceCode is not an allowed parameter.
	*
	*/
	private static boolean _validateCHPIXAddPersonSpec_Ch_pix_004_add_RaceCode(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getRaceCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of instance by a constraint : ch_pix_005_add_EthnicGroupCode
	* EthnicGroupCode is not an allowed parameter.
	*
	*/
	private static boolean _validateCHPIXAddPersonSpec_Ch_pix_005_add_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass){
		return ((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getEthnicGroupCode())).equals(new Integer(0));
		
	}

	/**
	* Validation of class-constraint : CHPIXAddPersonSpec
    * Verify if an element of type CHPIXAddPersonSpec can be validated by CHPIXAddPersonSpec
	*
	*/
	public static boolean _isCHPIXAddPersonSpec(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPIXAddPersonSpec
     * class ::  net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person
     * 
     */
    public static void _validateCHPIXAddPersonSpec(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass, String location, List<Notification> diagnostic) {
		if (_isCHPIXAddPersonSpec(aClass)){
			executeCons_CHPIXAddPersonSpec_Ch_pix_002_add_Name(aClass, location, diagnostic);
			executeCons_CHPIXAddPersonSpec_Ch_pix_003_add_ReligiousAffiliationCode(aClass, location, diagnostic);
			executeCons_CHPIXAddPersonSpec_Ch_pix_004_add_RaceCode(aClass, location, diagnostic);
			executeCons_CHPIXAddPersonSpec_Ch_pix_005_add_EthnicGroupCode(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPIXAddPersonSpec_Ch_pix_002_add_Name(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXAddPersonSpec_Ch_pix_002_add_Name(aClass) )){
				notif = new Info();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Info();
		}
		notif.setTest("ch_pix_002_add_Name");
		notif.setDescription("In the  name  field, the birth name is passed with the qualifier BR.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedadd-CHPIXAddPersonSpec-ch_pix_002_add_Name");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-002"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPIXAddPersonSpec_Ch_pix_003_add_ReligiousAffiliationCode(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXAddPersonSpec_Ch_pix_003_add_ReligiousAffiliationCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_003_add_ReligiousAffiliationCode");
		notif.setDescription("ReligiousAffiliationCode is not an allowed parameter.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedadd-CHPIXAddPersonSpec-ch_pix_003_add_ReligiousAffiliationCode");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-003"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPIXAddPersonSpec_Ch_pix_004_add_RaceCode(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXAddPersonSpec_Ch_pix_004_add_RaceCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_004_add_RaceCode");
		notif.setDescription("RaceCode is not an allowed parameter.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedadd-CHPIXAddPersonSpec-ch_pix_004_add_RaceCode");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-004"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPIXAddPersonSpec_Ch_pix_005_add_EthnicGroupCode(net.ihe.gazelle.hl7v3.prpamt201301UV02.PRPAMT201301UV02Person aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPIXAddPersonSpec_Ch_pix_005_add_EthnicGroupCode(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pix_005_add_EthnicGroupCode");
		notif.setDescription("EthnicGroupCode is not an allowed parameter.");
		notif.setLocation(location);
		notif.setIdentifiant("chpixfeedadd-CHPIXAddPersonSpec-ch_pix_005_add_EthnicGroupCode");
		notif.getAssertions().add(new Assertion("CH-PIX","CH-PIX-005"));
		diagnostic.add(notif);
	}

}
