package net.ihe.gazelle.hl7v3.validator.chpdqresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPDQControlActProcess
  * package :   chpdqresponse
  * Constraint Spec Class
  * class of test : PRPAIN201306UV02MFMIMT700711UV01ControlActProcess
  * 
  */
public final class CHPDQControlActProcessValidator{


    private CHPDQControlActProcessValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_014_controlActProcess
	* If there are more than 5 matches zero matches a special handling like in the XCPD transaction (see IHE ITI TF-2b, chapter 3.55.4.2.2.6) is necessary.
	*
	*/
	private static boolean _validateCHPDQControlActProcess_Ch_pdq_014_controlActProcess(net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (MFMIMT700711UV01Reason anElement1 : aClass.getReasonOf()) {
			    Boolean result2;
			    result2 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (MCAIMT900001UV01Requires anElement2 : anElement1.getDetectedIssueEvent().getTriggerFor()) {
			    	    if (!(((!(anElement2.getActOrderRequired() == null) && !(anElement2.getActOrderRequired().getCode() == null)) && !(((String) anElement2.getActOrderRequired().getCode().getCode()) == null)) && net.ihe.gazelle.gen.common.CommonOperationsStatic.matchesCodeToValueSet("1.3.6.1.4.1.12559.11.25.1.15.5", anElement2.getActOrderRequired().getCode().getCode().toLowerCase()))) {
			    	        result2 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!((!(anElement1.getDetectedIssueEvent() == null) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getDetectedIssueEvent().getTriggerFor()) > new Integer(0))) && result2)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getSubject()) > new Integer(0)) || ((((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getSubject())).equals(new Integer(0)) && !(aClass.getQueryAck() == null)) && !(aClass.getQueryAck().getResultTotalQuantity() == null)) && !(((Integer) aClass.getQueryAck().getResultTotalQuantity().getValue()) == null)) && ((Object) aClass.getQueryAck().getResultTotalQuantity().getValue()).equals(new Integer(0)))) || ((((Object) tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getSubject())).equals(new Integer(0)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getReasonOf()) > new Integer(0))) && result1));
		
		
	}

	/**
	* Validation of class-constraint : CHPDQControlActProcess
    * Verify if an element of type CHPDQControlActProcess can be validated by CHPDQControlActProcess
	*
	*/
	public static boolean _isCHPDQControlActProcess(net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQControlActProcess
     * class ::  net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess
     * 
     */
    public static void _validateCHPDQControlActProcess(net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQControlActProcess(aClass)){
			executeCons_CHPDQControlActProcess_Ch_pdq_014_controlActProcess(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQControlActProcess_Ch_pdq_014_controlActProcess(net.ihe.gazelle.hl7v3.prpain201306UV02.PRPAIN201306UV02MFMIMT700711UV01ControlActProcess aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQControlActProcess_Ch_pdq_014_controlActProcess(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_014_controlActProcess");
		notif.setDescription("If there are more than 5 matches zero matches a special handling like in the XCPD transaction (see IHE ITI TF-2b, chapter 3.55.4.2.2.6) is necessary.");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQControlActProcess-ch_pdq_014_controlActProcess");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-014"));
		diagnostic.add(notif);
	}

}
