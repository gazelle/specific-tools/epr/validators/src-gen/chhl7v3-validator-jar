package net.ihe.gazelle.hl7v3.validator.chpdqresponse;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.gen.common.CommonOperationsStatic;
import net.ihe.gazelle.hl7v3.datatypes.EnFamily;
import net.ihe.gazelle.hl7v3.datatypes.II;
import net.ihe.gazelle.hl7v3.mcaimt900001UV01.MCAIMT900001UV01Requires;
import net.ihe.gazelle.hl7v3.mfmimt700711UV01.MFMIMT700711UV01Reason;
import net.ihe.gazelle.hl7v3.datatypes.PN;



 /**
  * class :        CHPDQPersonalRelationship
  * package :   chpdqresponse
  * Constraint Spec Class
  * class of test : PRPAMT201310UV02PersonalRelationship
  * 
  */
public final class CHPDQPersonalRelationshipValidator{


    private CHPDQPersonalRelationshipValidator() {}



	/**
	* Validation of instance by a constraint : ch_pdq_013_Code
	* The personal relationship code SHALL be  FTH  or  MTH 
	*
	*/
	private static boolean _validateCHPDQPersonalRelationship_Ch_pdq_013_Code(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass){
		return ((!(aClass.getCode() == null) && !(((String) aClass.getCode().getCode()) == null)) && (aClass.getCode().getCode().equals("MTH") || aClass.getCode().getCode().equals("FTH")));
		
	}

	/**
	* Validation of instance by a constraint : ch_pdq_013_displayName
	* The allowed personal relationship display names are Father (for code FTH) and Mother (for code MTH)
	*
	*/
	private static boolean _validateCHPDQPersonalRelationship_Ch_pdq_013_displayName(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass){
		return (!(((String) aClass.getCode().getCode()) == null) && ((((String) aClass.getCode().getDisplayName()) == null) || ((aClass.getCode().getCode().equals("MTH") && aClass.getCode().getDisplayName().toLowerCase().equals("mother")) || (aClass.getCode().getCode().equals("FTH") && aClass.getCode().getDisplayName().toLowerCase().equals("father")))));
		
	}

	/**
	* Validation of class-constraint : CHPDQPersonalRelationship
    * Verify if an element of type CHPDQPersonalRelationship can be validated by CHPDQPersonalRelationship
	*
	*/
	public static boolean _isCHPDQPersonalRelationship(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHPDQPersonalRelationship
     * class ::  net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship
     * 
     */
    public static void _validateCHPDQPersonalRelationship(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass, String location, List<Notification> diagnostic) {
		if (_isCHPDQPersonalRelationship(aClass)){
			executeCons_CHPDQPersonalRelationship_Ch_pdq_013_Code(aClass, location, diagnostic);
			executeCons_CHPDQPersonalRelationship_Ch_pdq_013_displayName(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHPDQPersonalRelationship_Ch_pdq_013_Code(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPersonalRelationship_Ch_pdq_013_Code(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("ch_pdq_013_Code");
		notif.setDescription("The personal relationship code SHALL be  FTH  or  MTH");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_Code");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-013"));
		diagnostic.add(notif);
	}

	private static void executeCons_CHPDQPersonalRelationship_Ch_pdq_013_displayName(net.ihe.gazelle.hl7v3.prpamt201310UV02.PRPAMT201310UV02PersonalRelationship aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHPDQPersonalRelationship_Ch_pdq_013_displayName(aClass) )){
				notif = new Warning();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Warning();
		}
		notif.setTest("ch_pdq_013_displayName");
		notif.setDescription("The allowed personal relationship display names are Father (for code FTH) and Mother (for code MTH)");
		notif.setLocation(location);
		notif.setIdentifiant("chpdqresponse-CHPDQPersonalRelationship-ch_pdq_013_displayName");
		notif.getAssertions().add(new Assertion("CH-PDQ","CH-PDQ-013"));
		diagnostic.add(notif);
	}

}
